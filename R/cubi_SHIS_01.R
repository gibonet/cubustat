#' Studenti ticinesi delle università e dei politecnici della Svizzera, secondo la sede e l'ambito di studio, dal semestre autunnale 2000/01 al 2023/24
#' 
#' 
#' Fonte: Studenti ed esami delle scuole universitarie (SHIS-studex), Ufficio federale di statistica, Neuchâtel
#' https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.fonti&tema=36&id=944&proID=145
#' 
#' 
#' Elaborazione: Ufficio di statistica (Ustat), Giubiasco
#' 
#' Ultima modifica: 11.07.2024
#' 
#' Variabili presenti nel cubo di dati:
#' Semestre: il semestre accademico autunnale; fino al 2005/06 si tratta del semestre invernale
#' Sede: la sede dell'università o del politecnico
#' Ambito_di_studio: l'ambito in cui lo studente è in formazione
#' 
#' 
#' Glossario:
#' Studenti universitari: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&id=109&tema=
#' 
#' Ogni riga del file csv contiene una determinata statistica che si riferisce a una determinata combinazione di caratteristiche.
#' I risultati statistici sono nella colonna "Numero".
#' La prima riga del file csv contiene i nomi delle colonne del cubo di dati.
#' Ogni colonna è separata da un punto e virgola (;).
"cubi_SHIS_01"

