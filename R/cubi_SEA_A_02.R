#' Abitazioni in edifici abitativi, secondo il tipo di edificio, l'epoca di costruzione, il numero di locali e la superficie, per comune, in Ticino, dal 2014 al 2023
#' 
#' Fonte: Statistica degli edifici e delle abitazioni (SEA), Ufficio federale di statistica, Neuchâtel
#' Elaborazione: Ufficio di Statistica (Ustat), Giubiasco
#' Versione dei dati: 23.09.2024
#' 
#' Ultima modifica: 23.09.2024
#' 
#' https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.fonti&tema=46&id=344&proID=316
#' 
#' Variabili presenti nel cubo di dati: 
#' Anno: anno del rilevamento (al 31.12)
#' Comune: nome del comune dove si trova l'edificio (stato dei comuni politici - 106, dal 10.04.2022)
#' Tipo_edificio: tipo di edificio ad uso abitativo
#' Epoca_costruzione: epoca di costruzione dell'edificio
#' Numero_locali: numero di locali dell'abitazione
#' Superficie: superficie dell'abitazione in metri quadrati
#' 
#' Descrizione delle statistiche:
#' Abitazioni: numero di abitazioni
#' 
#' La colonna "info" contiene una delle informazioni seguenti: 
#' ok: il dato è disponibile per l'incrocio delle variabili selezionate
#' ...: dato non disponibile o senza senso
#' ( ): dato non pubblicato per insufficiente attendibilità statistica
#' (cifra): affidabilità statistica del dato relativa (basato su un numero troppo basso di osservazioni)
#' X: dato non pubblicato per motivi legati alla protezione dei dati
#' 
#' Nota:
#' Per contenere le dimensioni del cubo di dati, è proposta una serie storica dal 2014 in poi. Su richiesta, i dati sono disponibili dal 2009.
#' 
#' Segni, simboli, abbreviazioni, sigle e concetti statistici usati nei prodotti dell'Ustat: http://www4.ti.ch/fileadmin/DFE/DR-USTAT/Prodotti/Definizioni/segni_sigle_simboli.pdf
#' 
#' Glossario: 
#' Tipo di edificio ad uso abitativo: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&id=2644&tema=46
#' Abitazioni: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&id=2641&tema=46
#' Epoca di costruzione: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&id=378&tema=46
#' Locali: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&id=571&tema=46
#' Superficie: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&id=96&tema=46
#' 
#' 
"cubi_SEA_A_02"

