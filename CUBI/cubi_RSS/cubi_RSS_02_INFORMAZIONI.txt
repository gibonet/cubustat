Salario mensile lordo standardizzato (in fr.) e altri indicatori nel settore privato, secondo la divisione economica (NOGA 2008), il sesso, la posizione nella professione, il grado di formazione e la residenza (residenti, frontalieri), in Ticino, dal 2008 al 2022

Fonte: Rilevazione della struttura dei salari (RSS), Ufficio federale di statistica, Neuchâtel
Elaborazione: Ufficio di statistica (Ustat), Giubiasco

https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.fonti&tema=35&id=64&proID=178

Ultima modifica: 14.11.2024

Versione dei dati: 05.11.2024

Variabili presenti nel cubo di dati:
anno: l'anno dell'inchiesta
noga08_2_descr: la divisione economica (NOGA 2008)
sesso: il sesso
posizione: la posizione nella professione
formazione2: il grado di formazione
residenti_2: la residenza (residenti, frontalieri)


Descrizione delle statistiche:
AD_salariati: addetti ai sensi della RSS
ETP_salariati: addetti ETP ai sensi della RSS
p10: decimo percentile del salario mensile lordo standardizzato (primo decile, in franchi)
p25: venticinquesimo percentile del salario mensile lordo standardizzato (primo quartile, in franchi)
p50: cinquantesimo percentile del salario mensile lordo standardizzato (mediana, in franchi)
p75: settantacinquesimo percentile del salario mensile lordo standardizzato (terzo quartile, in franchi)
p90: novantesimo percentile del salario mensile lordo standardizzato (nono decile, in franchi)


La colonna "info" riporta una delle informazioni seguenti:
ok: esistono delle stime con le caratteristiche descritte in quella riga
…: dato non disponibile
X: dato non pubblicato per motivi legati alla protezione dei dati
( ): coefficiente di variazione superiore a 5% (valore incerto a livello statistico)


Segni, simboli, abbreviazioni, sigle e concetti statistici usati nei prodotti dell'Ustat: http://www4.ti.ch/fileadmin/DFE/DR-USTAT/Prodotti/Definizioni/segni_sigle_simboli.pdf

Glossario:
Salario mensile lordo standardizzato: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&tema=35&id=191
Addetti ai sensi della RSS: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&tema=35&id=1881
Addetti ETP ai sensi della RSS: https://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=definizioni.glossario&tema=35&id=1882


Ogni riga del file csv contiene una determinata statistica che si riferisce a una determinata combinazione di caratteristiche.
I risultati statistici sono nella colonna "valore".
La prima riga del file csv contiene i nomi delle colonne del cubo di dati.
Ogni colonna è separata da un punto e virgola (;).

Codifica/encoding del file csv: UTF-8
